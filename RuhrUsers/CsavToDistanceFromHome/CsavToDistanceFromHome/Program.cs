﻿using Accord.MachineLearning;
using Accord.Statistics.Distributions.DensityKernels;
using Geolocation;
using Nest;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CsavToDistanceFromHome
{
	public class DistanceHomeData
	{
		public string userid { get; set; }
		[GeoPoint]
		public Coordinates coord { get; set; }
		public double distance { get; set; }
		public double timespentonlocation { get; set; }
		public int daysSpentinTotal { get; set; }
	}

	public class Coordinates
	{
		[Number(NumberType.Double, Name = "lat")]
		public double Lat { get; set; }

		[Number(NumberType.Double, Name = "lon")]
		public double Lon { get; set; }
	}
	class RTBData
	{
		[GeoPoint]
		public Coordinates location { get; set; }
		public DateTime time { get; set; }
		public string userid { get; set; }
	}

	class Program
	{
		static void Main(string[] args)
		{
			var locationName = "essen";

			var locationRuhrpark = new Coordinate()//Ruhrpark
			{
				Latitude = 51.4946,
				Longitude = 7.279
			};

			var locationLimbecker = new Coordinate()//Limbecker Platz
			{
				Latitude = 51.457955,
				Longitude = 7.0050461
			};

			var ekz = locationLimbecker;


			var rtbdata = new List<RTBData>();
			var csv = File.ReadLines("C:\\Priya\\newdatei\\user_" + locationName + ".csv");
			foreach (var x in csv)
			{
				try
				{
					var cols = x.Split(';');
					rtbdata.Add(new RTBData
					{
						location = new Coordinates { Lat = Double.Parse(cols[2], CultureInfo.InvariantCulture), Lon = Double.Parse(cols[3], CultureInfo.InvariantCulture) },
						userid = cols[1],
						time = DateTime.Parse(cols[0]),
					});
				}
				catch
				{

				}
			}

			var users = rtbdata
				.Take(766052)
				.GroupBy(x => x.userid)
				.OrderBy(x => x.Count())
				.Take(5150)
				.Select(a => a.First().userid)
				.ToList();

			var DataOfMostDataUsers = rtbdata.Where(x => users.Contains(x.userid))
				.Where(x => x.location.Lat > 0 && x.location.Lon > 0);

			var csvUsers = String.Join('\n', DataOfMostDataUsers.Select(x => String.Join(';', x.userid, x.time, x.location.Lat, x.location.Lon)));
			File.WriteAllText("C:\\Priya\\newdatei\\5150Users.csv", csvUsers);





		}
	}
}
